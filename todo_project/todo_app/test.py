from django.test import TestCase
from todo_project.todo_app.models import Todo


class AnimalTestCase(TestCase):
    def setUp(self):
        Todo.objects.create(name="First Todo", description="Test description 1")
        Todo.objects.create(name="Second Todo", description="Test description 2")