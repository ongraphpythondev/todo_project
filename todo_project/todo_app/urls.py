from django.urls import path
from .views import *


urlpatterns = [

    path('todos/', TodoListView.as_view()),
    path('todo/<int:pk>/', TodoDetailView.as_view()),

]